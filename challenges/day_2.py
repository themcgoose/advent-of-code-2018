# themcgoose
# Advent-Of-Code 2019

# https://adventofcode.com/2018/day/2

import click


def count_two_and_three_patterns(input_string):

    exactly_two = 0
    exactly_three = 0

    uniq_chars = set(list(input_string))

    for char in uniq_chars:
        cnt = input_string.count(char)
        if cnt == 2:
            exactly_two = 1
        if cnt == 3:
            exactly_three = 1

    # print(f"String {input_string}, Chars: {uniq_chars} -- ({exactly_two}, {exactly_three})")

    return exactly_two, exactly_three

def greedy_string_comparisons(str_a, str_b):
    str_a = list(str_a)
    str_b = list(str_b)

    chars_different = 0
    for idx, val_a in enumerate(str_a):
        if val_a != str_b[idx]:
            chars_different += 1
        
        if chars_different > 1:
            return False
    
    if chars_different == 1:
        return True
    else:
        return False


@click.command()
@click.option('-f', '--file',
              help='Filename to read in')
@click.option('-i', '--input_str', help='Input from command line, comma seperated list of operations')
def main(file=None, input_str=None):
    # Read Input
    if (file is None and input_str is None) or (file is not None and input_str is not None):
        raise(RuntimeError(
            "Error, must provide either a file or string input, but not none or both"))

    elif file:
        with open(file) as f:
            f_data = f.readlines()
        data = [item.rstrip('\n') for item in f_data]

    elif input_str:
        data = input_str.split(',')
        data = [item.replace(" ", "") for item in data]
        print(f"Got {data} from input")

    # stdin and file input should now be in same format
    two_cnt = 0
    three_cnt = 0

    for code in data:
        tmp_two, tmp_three = count_two_and_three_patterns(code)
        two_cnt += tmp_two
        three_cnt += tmp_three

    print(f"Found ({two_cnt}, {three_cnt})")
    print(f"Part 1 Solution: {two_cnt * three_cnt}")

    # Finding Boxes that are one character apart
    # Pop item from list
    # Check against remaning items if only one character apart

    off_by_one_str = []
    continue_iteration = True

    while continue_iteration:
        to_compare = data.pop()
        for item in data:
            result = greedy_string_comparisons(to_compare, item)
            if result:
                off_by_one_str.append(to_compare)
                off_by_one_str.append(item)
                continue_iteration = False
                break
    
    print(f"Found {off_by_one_str} - removing differing character")
    print("Part 2 Solution: ", end='')
    off_by_one_str = [list(item) for item in off_by_one_str]
    assert len(off_by_one_str) == 2
    for idx, char in enumerate(off_by_one_str[0]):
        if off_by_one_str[1][idx] == char:
            print(char, end='')
    print("\n", end='')




if __name__ == '__main__':
    main()
