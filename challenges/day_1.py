# themcgoose
# Advent-Of-Code 2019

# https://adventofcode.com/2018/day/1

import click

def loop_frequencies(frequencies, current_frequency, freq_set):
    for item in frequencies:
        current_frequency += item
        if current_frequency in freq_set:
            return (True, current_frequency)
        else:
            freq_set.add(current_frequency)
        print(f"Value: {current_frequency} Size of Set {len(freq_set)}")
    
    return (False, current_frequency)
    


@click.command()
@click.option('-f', '--file',
              help='Filename to read in')
@click.option('-i', '--input_str', help='Input from command line, comma seperated list of operations')
def main(file=None, input_str=None):
    # Read Input
    if (file is None and input_str is None) or (file is not None and input_str is not None):
        raise(RuntimeError(
            "Error, must provide either a file or string input, but not none or both"))

    elif file:
        with open(file) as f:
            f_data = f.readlines()
        data = [item.rstrip('\n') for item in f_data]

    elif input_str:
        data = input_str.split(',')
        data = [item.replace(" ", "") for item in data]

    # Data from either file or input now in same format
    data = [int(item) for item in data]
    
    freq = 0
    frequncies_seen = set()
    frequncies_seen.add(0)
    continue_looping = True

    while continue_looping:
        (found_twice, freq) = loop_frequencies(data, freq, frequncies_seen)
        if found_twice:
            break

    print(freq)


if __name__ == '__main__':
    main()